## Vian

![banner](https://i.pinimg.com/originals/d1/02/d6/d102d6c1f5c4997c7b268b3eda173e86.gif)

Just another linux enthusiast.

Tools i use: [dotfiles](https://gitlab.com/idjo/dotfiles)

<details>

<summary>Stats 🌲</summary>

![cocatrip's GitHub stats](https://github-readme-stats.vercel.app/api?username=cocatrip&show_icons=true&theme=merko)

![Most Used Language stats](https://github-readme-stats.vercel.app/api/top-langs/?username=cocatrip&layout=compact&theme=merko&hide=html,css&exclude_repo=dotfiles,onedark.nvim,rms-support-letter.github.io)

</details>
